using System.Collections.Generic;
using UnityEngine;

namespace UDev.WindowManager
{
    ///<summary>
    /// Window Manager
    ///</summary>
    public class WindowAgregator : MonoBehaviour
    {
        [SerializeField] 
        private WindowData firstWindow = default;
        [SerializeField] 
        private List<WindowData> windowList = new List<WindowData>(); // Window List

        private List<Window> poolWindow = default; //Prefab Pool

        private Window previousWindow = default; // Previous Window

        public static string windowName = "menu";
        private void Awake()
        {
            poolWindow = new List<Window>();
            windowName = firstWindow.Id;
            OpenWindow(true);
        }

        private void OnEnable() => WindowButton.onWindowChangeStatus += OpenWindow;

        private void OnDisable() => WindowButton.onWindowChangeStatus -= OpenWindow;

        private void OpenWindow(bool status)
        {
            
            if (previousWindow != null && windowList.Find(w => w.Id == previousWindow.Id).IsNeedClose)
            {
                previousWindow.gameObject.SetActive(false);
            }

            WindowData windowInScene = windowList.Find(window => window.Id == windowName);

            if (!poolWindow.Find(window => window.Id == windowInScene.Id)) 
            {
                GameObject activeWindow = Instantiate(windowInScene.WindowPrefab.gameObject, gameObject.transform);
                poolWindow.Add(activeWindow.GetComponent<Window>());
                poolWindow[poolWindow.Count-1].Id = windowInScene.Id;
                SetStatus(activeWindow, status);
            }
            else
            {
                Window windowInPool = poolWindow.Find(window => window.Id == windowName);
                SetStatus(windowInPool.gameObject, status);
            }
        }

        private void SetStatus(GameObject window, bool status)
        {
            window.SetActive(status);
            window.transform.SetSiblingIndex(1);
            previousWindow = window.GetComponent<Window>();
        }
    }
}