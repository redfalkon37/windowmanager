using UnityEngine;

namespace UDev.WindowManager
{
    ///<summary>
    /// Interface window
    ///</summary>
    [CreateAssetMenu(fileName = "WindowPrefab", menuName = "Window/WindowPrefab", order = 1)]
    public class WindowData : ScriptableObject
    {
        [SerializeField] 
        private string id = default;
        [SerializeField] 
        private Window windowPrefab = default;
        [SerializeField]
        private bool isNeedClose = false;

        /// <summary>
        /// window id
        /// </summary>
        public string Id => id;

        /// <summary>
        /// window prefab
        /// </summary>
        public Window WindowPrefab => windowPrefab;

        /// <summary>
        /// Is need close Previous window
        /// </summary>
        public bool IsNeedClose => isNeedClose;
    }
}