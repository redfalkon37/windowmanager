using System;
using UnityEngine;

namespace UDev.WindowManager
{
    /// <summary>
    /// Window Button
    /// </summary>
    public class WindowButton : AbstractButtonView
    {
        /// <summary>
        /// window Event
        /// </summary>
        public static event Action<bool> onWindowChangeStatus = delegate { };

        /// <summary>
        /// Window Action
        /// </summary>
        public WindowAction action = WindowAction.Open;

        [SerializeField] 
        private WindowData window = default;

        public override void OnButtonClicked()
        {
            WindowAgregator.windowName = window.Id;
            onWindowChangeStatus(action == WindowAction.Open);
        }
    }

    /// <summary>
    /// Window Action enum
    /// </summary>
    public enum WindowAction
    {
        Open,
        Close
    }
}