using UnityEngine;

namespace UDev.WindowManager
{
    /// <summary>
    /// Window class
    /// </summary>
    public class Window : MonoBehaviour
    {
        public string Id { get; set; }
    }
}